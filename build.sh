#!/usr/bin/env bash
##
## A simple script to build documentation from markdown files.
##
## Copyright [2019-2020], [Libertarian Tech]
##
## requires: pandoc


version=0.0.5

arg1=${1:-}
arg2=${2:-}
meName=${0##*/}
usage="$meName $version [help]"


_help () {
  printf '%s\n' "$usage"
  printf '%s\n' "help        -h        Print this message."
  declare -F | awk '{print $3}' | grep -v "^_"
}

build (){ 
  ## Build pdf and epub.
  cd ${PWD##*/}/ && {
  find ./ -iname "*.md" -type f -exec sh -c 'pandoc "${0}" -o "${0%.md}.pdf"' {} \; 
  find ./ -iname "*.md" -type f -exec sh -c 'pandoc "${0}" -o "${0%.md}.epub"' {} \;
  # find ./ -iname "*.md" -type f -exec sh -c 'pandoc --toc "${0}" -o "${0%.md}.odt"' {} \;
  } || mkdir -p ${PWD##*/}
}

[[ $arg1 == -h || $arg1 == *help* ]] && { _help && exit 0 ;}

build
