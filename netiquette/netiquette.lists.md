% Discussion List Netiquette
% IS Committee, Libertarian Party of Pennsylvania
% April 17, 2020, v0.1

# LPPA lists - Netiquette

This Netiquette document applies on lppa* lists that do not have their own policies documented.

  - Don't abuse your power.
  - Try not to be excessively off-topic.
      Try to keep the discussion roughly related to the topic of the list, especially if you are not (yet) a regular. 
  - Identify yourself, if you participate in discussions.
       We don't want to invade your privacy but we like to know who we are talking to so we can recognize you on mailing lists. Putting your identity into the name field is a good idea. 
  - Don't feed the trolls.
  - No public logging.
  - Respect other people's time and bandwidth.
  - Don't argue against the rules.
      This includes arguing over actions taken by list moderators. If you feel harassed by moderators, PM them politely, or try rejoining tomorrow. 
  - Share expert knowledge.
  - If you have a question, don't ask to ask, just come and state your question.
  - Be helpful.
  - Have fun.
